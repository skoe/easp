#!/bin/bash

# only set HOST for the target platform when cross compiling, with f.e. mingw
#HOST=i686-w64-mingw32

### nothing to edit below ###

if [ ! -z $HOST ]; then
  CC=$HOST-gcc
else
  CC=gcc
  HOST=`uname -s`_`uname -m`
fi

CFLAGS="-O -Wall"
LDFLAGS=""
LDFLAGS_STATIC="$LDFLAGS -static -Bstatic"

OBJ=easp.o

make_o() {
  $CC $CFLAGS -c -o $OBJ easp.c
}

make_clean() {
  rm -f $OBJ easp easp-*
}

make_dist() {
  tar cjvf ../easp-`date --rfc-3339=date`.tar.bz2 --numeric-owner \
	    --exclude=".hg*" --exclude="*.svf" ../${PWD##/*/}
}

make_dynamic() {
  make_o
  $CC $LDFLAGS -o easp $OBJ -lftdi
}

make_static() {
    make_o
    $CC $LDFLAGS_STATIC -o easp-$HOST easp.o -lftdi -lusb 
}

case $1 in
  static)
    make_static
  ;;

  clean)
    make_clean
  ;;

  dist)
    make_dist
  ;;
  dist-clean)
    make_clean
    make_dist
  ;;
  *)
    make_dynamic
  ;;
esac
