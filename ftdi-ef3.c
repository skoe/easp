/* ftdi-ef3.c

   Initialise the FT245R to be used in the Easyflash3

   This program is distributed under the GPL, version 2
*/

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ftdi.h>

static const unsigned char ef3init[] = {
  0x01, 0x40, 0x03, 0x04, 0x38, 0x87, 0x00, 0x06, 0x80, 0x2d, 0x08, 0x00, 0x00, 0x00, 0x98, 0x0a, /* |.@..8....-......| */
  0xa2, 0x18, 0xba, 0x12, 0x23, 0x10, 0x05, 0x00, 0x0a, 0x03, 0x73, 0x00, 0x6b, 0x00, 0x6f, 0x00, /* |....#.....s.k.o.| */
  0x65, 0x00, 0x18, 0x03, 0x45, 0x00, 0x61, 0x00, 0x73, 0x00, 0x79, 0x00, 0x46, 0x00, 0x6c, 0x00, /* |e...E.a.s.y.F.l.| */
  0x61, 0x00, 0x73, 0x00, 0x68, 0x00, 0x20, 0x00, 0x33, 0x00, 0x12, 0x03, 0x41, 0x00, 0x36, 0x00, /* |a.s.h. .3...A.6.| */
  0x56, 0x00, 0x36, 0x00, 0x4a, 0x00, 0x36, 0x00, 0x54, 0x00, 0x34, 0x00, 0x00, 0x00, 0x00, 0x00, /* |V.6.J.6.T.4.....| */
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xd9, 0x2d
};

static void usage(const char* prg)
{
    fprintf(stderr, 
    	    "Usage: %s <cmd>\n"
            "    <cmd> is one of:\n"
            "    read <filename>    Write a binary dump of FTDI chip EEPROM to file\n"
            "    prog-ef3           Program EasyFlash3 defaults to FTDI chip\n",
            prg);
}


static int read_eeprom(struct ftdi_context* ftdi, const char* filename)
{
    FILE *fp;
    unsigned char *eeprom = NULL;
    int size;

    fp = fopen(filename, "wb");
    if (fp == NULL)
    {
        fprintf(stderr, "Failed to write %s.\n", filename);
        return EXIT_FAILURE;
    }

    if (ftdi_read_eeprom(ftdi) < 0)
    {
        fprintf(stderr, "Failed to read EEPROM: (%s)\n", 
                ftdi_get_error_string(ftdi));
        return EXIT_FAILURE;
    }

    if (ftdi_get_eeprom_value(ftdi, CHIP_SIZE, &size) < 0)
    {
        fprintf(stderr, "Failed to get EEPROM size: (%s)\n", 
                ftdi_get_error_string(ftdi));
        return EXIT_FAILURE;
    }

    if ((eeprom = calloc(size, 1)) == NULL)
    {
      fprintf(stderr, "Could not allocate EEPROM buffer\n");
      return EXIT_FAILURE;
    }

    if (ftdi_get_eeprom_buf(ftdi, eeprom, size) < 0)
    {
      fprintf(stderr, "Could not get EEPROM data: (%s)\n",
              ftdi_get_error_string(ftdi));
      return EXIT_FAILURE;

    }

    fwrite(eeprom, size, 1, fp);

    printf("OK.\n");
    return EXIT_SUCCESS;
}

static int write_ef3(struct ftdi_context* ftdi)
{
    if(ftdi_eeprom_initdefaults(ftdi, NULL, NULL, NULL))
    {
      fprintf(stderr, "Failed to initialise ftdi->eeprom: (%s)\n",
      ftdi_get_error_string(ftdi));
      return EXIT_FAILURE;
    }

    if (ftdi_read_eeprom(ftdi) < 0)
    {
        fprintf(stderr, "Failed to read EEPROM: (%s)\n", 
                ftdi_get_error_string(ftdi));
        return EXIT_FAILURE;
    }

    if (ftdi_set_eeprom_buf(ftdi, ef3init, sizeof(ef3init)) < 0)
    {
        fprintf(stderr, "Failed to fill EEPROM data: (%s)\n", 
                ftdi_get_error_string(ftdi));
        return EXIT_FAILURE;
    }

    if (ftdi_write_eeprom(ftdi) < 0)
    {
        fprintf(stderr, "Failed to write EEPROM: (%s)\n", 
                ftdi_get_error_string(ftdi));
        return EXIT_FAILURE;
    }

    printf("OK.\n");
    return EXIT_SUCCESS;
}


int main(int argc, char** argv)
{
    const char* filename = "";
    int do_prog;
    int ret, rv;
    struct ftdi_context *ftdi = NULL;

    printf("\n  EasyFlash3 FT245R programmer V10.0\n\n");

    if (argc == 3 && strcmp(argv[1], "read") == 0)
    {
        do_prog = 0;
        filename = argv[2];
    }
    else if (argc == 2 && strcmp(argv[1], "prog-ef3") == 0)
    {
        do_prog = 1;
    }
    else
    {
        usage(argv[0]);
        return 1;
    }

    if ((ftdi = ftdi_new()) == NULL)
    {
        fprintf(stderr, "ftdi initialisation failed\n");
        return EXIT_FAILURE;
    }

    if ((ret = ftdi_usb_open(ftdi, 0x0403, 0x8738)) < 0)
    {
        if ((ret = ftdi_usb_open(ftdi, 0x0403, 0x6001)) < 0)
        {
            fprintf(stderr, "Failed to open ftdi device: %d (%s)\n", ret, ftdi_get_error_string(ftdi));
            return EXIT_FAILURE;
        }
    }

    if (ftdi->type != TYPE_R)
    {
        fprintf(stderr, "Wrong FTDI type (only type R supported)\n");
        return EXIT_FAILURE;
    }

    if (do_prog)
        rv = write_ef3(ftdi);
    else
        rv = read_eeprom(ftdi, filename);

    if ((ret = ftdi_usb_close(ftdi)) < 0)
    {
        fprintf(stderr, "Failed to close ftdi device: %d (%s)\n", ret, ftdi_get_error_string(ftdi));
        return EXIT_FAILURE;
    }

    ftdi_deinit(ftdi);
    ftdi_free(ftdi);

    return rv;
}
