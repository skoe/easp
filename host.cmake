# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Windows)

# which compilers to use for C and C++
SET(CMAKE_C_COMPILER ${HOST}-gcc)
SET(CMAKE_CXX_COMPILER ${HOST}-g++)

# here is the target environment located
SET(CMAKE_FIND_ROOT_PATH  /usr/${HOST})

# adjust the default behaviour of the FIND_XXX() commands:
# search headers and libraries in the target environment, search 
# programs in the host environment
set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

set(CMAKE_RC_COMPILER ${HOST}-windres)
set(LIBUSB_INCLUDE_DIR ${USBDIR})
